#ifndef list_H_
#define list_H_

union data{
	char *c;
	int id;
	int duration;
	int lines;
};

typedef struct node{
    union data value;
    struct node *next;
    
} node_body;

typedef struct linkedList{
    int total;
    node_body *start;
    
} linkedList_body;

#endif