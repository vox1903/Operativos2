
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

#include "commons.h"
#include "structures.h"

line_body array[20];
int pages = 20;
int countP = 1;

bool firstFit(proc_args *procArgs){

    int space = procArgs->lines;
    int id = procArgs->id;
    int temp,temp2;

    //CANT FIT THE PROCESS
    if (space > pages){return false;}

    for (int i = 0; i < pages; i++){
        temp = i;
        temp2 = 0;
        while (array[temp].id == -1 && temp < pages){
            temp2 ++;
            temp ++;
        }
        if (temp2 >= space){
            temp = i;
	        temp2 = space;
	        while (temp2 != 0){
	            array[temp].id = id;
	            temp2 --;
	            temp ++;
	        }
            return true;
        }
    } 
    return false;
}


bool bestFit(proc_args *procArgs){

    int space = procArgs->lines;
    int id = procArgs->id;
    int temp,temp2,best,bestStart;

    best = 0;
    //CANT FIT THE PROCESS
    if (space > pages){return false;}

    for (int i = 0; i < pages; i++){
        //if (pages - i + 1 < space){return false;}
        temp = i;
        temp2 = 0;
        while (array[temp].id == -1 && temp < pages){
            temp2 ++;
            temp ++;
        }
        if (temp2 >= space &&(best == 0 || best > temp2)){
            best = temp2;
            bestStart = i;
        }

    }
    if (best == 0){return false;}

    for (int i = 0; i < temp; i++){
        array[bestStart].id = id;
        bestStart ++;
    }

    return true;

}

bool worstFit(proc_args *procArgs){

    int space = procArgs->lines;
    int id = procArgs->id;
    int temp,temp2,best,bestStart;

    best = 0;
    //CANT FIT THE PROCESS
    if (space > pages){return false;}

    for (int i = 0; i < pages; i++){
        //if (pages - i + 1 < space){return false;}
        temp = i;
        temp2 = 0;
        while (array[temp].id == -1 && temp < pages){
            temp2 ++;
            temp ++;
        }
        if (temp2 >= space &&(best == 0 || best < temp2)){
            best = temp2;
            bestStart = i;
        }

    }
    if (best == 0){return false;}

    for (int i = 0; i < temp; i++){
        array[bestStart].id = id;
        bestStart ++;
    }

    return false;
}


void printTable(){
	for (int i = 0; i < pages; i++){
		printf("Pos : %d val : %d \n", i,array[i].id);
	}
}


int main(int argc, char const *argv[]){
	
	proc_args *procArgs;

	srand(time(NULL));

	for (int i = 0; i < pages; i++)
	{
		array[i].id = -1;
	}

	while (true){
		procArgs = malloc(sizeof(proc_args));
        procArgs->id        = countP;
        procArgs->lines     = rand() %11+1;
        procArgs->duration  = rand() %15+10;

        printf("Id : %d Lines : %d\n", procArgs->id  , procArgs->lines );

        firstFit(procArgs);

        countP ++;

        printTable();

		sleep(5);
	}


	return 0;
}