#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>


#include "commons.h"
#include "structures.h"

//==========================GLOBAL==========================
pthread_t threadsID[500];
bool threadsFlag[500];

states_body *states;
line_body *procedures;
proc_args *dataWaiting;
proc_args *dataExecute;
int pages, algorithmSelected, countP,pagesWaitingList, pagesExecList;

//==========================


void writeTable(){
    char buffer[200];

    writelnLog("============TABLE============");
    for (int i = 0; i < pages; i++){
        sprintf(buffer,"Line %d : Process id %d ", i , procedures[i].id );
        writelnLog(buffer);
    } 
    writelnLog("============END TABLE============");
}

void writeWaitingList(){
    char buffer[200];

    writelnLog("============WAITING TABLE============");
    for (int i = 0; i < pagesWaitingList; i++){
        sprintf(buffer,"Line %d : Process id %d ", i , dataWaiting[i].id );
        writelnLog(buffer);
    } 
    writelnLog("============END TABLE============");
}

void writeExecList(){
    char buffer[200];

    writelnLog("============EXECUTION TABLE============");
    for (int i = 0; i < pagesExecList; i++){
        sprintf(buffer,"Line %d : Process id %d ", i , dataExecute[i].id );
        writelnLog(buffer);
    } 
    writelnLog("============END TABLE============");
}

void printTable(){

    printf("============TABLE============\n");
    for (int i = 0; i < pages; i++){
        printf("Line %d : Process id %d \n", i , procedures[i].id );
    } 
    printf("============END TABLE============\n");

}

void printWaitingList(){

    printf("============WAITING LIST============\n");
    for (int i = 0; i < pagesWaitingList; i++){
        printf("Line %d : Process id %d \n", i , dataWaiting[i].id );
    } 
    printf("============END TABLE============\n");

}

void printExecList(){

    printf("============EXECUTION TABLE============\n");
    for (int i = 0; i < pages; i++){
        printf("Line %d : Process id %d \n", i , dataExecute[i].id );
    } 
    printf("============END TABLE============\n");

}

void clearProc(int id){

    for (int i = 0; i < pages; i++){
        if (procedures[i].id == id){
            procedures[i].id = -1;
        }
    }
    printf("Clearing process : %d\n", id);
}


bool firstFit(proc_args *procArgs){

    int space = procArgs->lines;
    int id = procArgs->id;
    int temp,temp2;

    //CANT FIT THE PROCESS
    if (space > pages){return false;}

    for (int i = 0; i < pages; i++){
        temp = i;
        temp2 = 0;
        while (procedures[temp].id == -1 && temp < pages){
            temp2 ++;
            temp ++;
        }
        if (temp2 >= space){
            temp = i;
            temp2 = space;
            while (temp2 != 0){
                procedures[temp].id = id;
                temp2 --;
                temp ++;
            }
            return true;
        }
    } 
    return false;
}


bool bestFit(proc_args *procArgs){

    int space = procArgs->lines;
    int id = procArgs->id;
    int temp,bestPos,count,pos;;

    int best = 0;
    //CANT FIT THE PROCESS
    if (space > pages){return false;}

    int i = 0;
    while (i < pages){
        count = 0;
        temp = i;
        while (procedures[i].id == -1 && i < pages){
            count ++;
            i ++;
        }
        if (count >= space &&(best == 0 || best > count)){
            best = count;
            bestPos = temp;
        }
        i ++;
    }

    if (best == 0){return false;}

    for (int i = 0; i < space; i++){
        procedures[bestPos].id = id;
        bestPos ++;
    }

    return true;

}

bool worstFit(proc_args *procArgs){

    
    int space = procArgs->lines;
    int id = procArgs->id;
    int temp,bestPos,count,pos;;

    int best = 0;
    //CANT FIT THE PROCESS
    if (space > pages){return false;}

    int i = 0;
    while (i < pages){
        count = 0;
        temp = i;
        while (procedures[i].id == -1 && i < pages){
            count ++;
            i ++;
        }
        if (count >= space &&(best == 0 || best < count)){
            best = count;
            bestPos = temp;
        }
        i ++;
    }

    if (best == 0){return false;}

    for (int i = 0; i < space; i++){
        procedures[bestPos].id = id;
        bestPos ++;
    }

    return true;
}


void insertProcWaiting(int pId){
    int id = pId;

    for (int i = 0; i < pagesWaitingList; i++){
        if (dataWaiting[i].id == -1){
            dataWaiting[i].id = id;
        }
    } 
}

void insertProcExec(int pId){
    int id = pId;

    for (int i = 0; i < pagesExecList; i++){
        if (dataExecute[i].id == -1){
            dataExecute[i].id = id;
        }
    } 
}

void deleteProcWaiting(int pId){
    int id = pId;

    for (int i = 0; i < pagesWaitingList; i++){
        if (dataWaiting[i].id == id){
            dataWaiting[i].id = -1;
        }
    } 
}

void deleteProcExec(int pId){
    int id = pId;

    for (int i = 0; i < pagesExecList; i++){
        if (dataExecute[i].id == id){
            dataExecute[i].id = -1;
        }
    } 
}


//THREAD PROCESS
void *simulateProc(void * args){
    char buffer[200];
    proc_args *procArgs = args;
    bool storedFlag = false;    //CHECK IF PROCESS WAS SUSPENDED
    int id = procArgs->id;

    threadsFlag[id] = true;

    sem_wait(&states->semWaitingList);
    insertProcWaiting(id);
    sem_post(&states->semWaitingList);

    //WAIT TO ADD THE PROC
    sem_wait(&states->semProc);

    if (algorithmSelected == BESTFIT){
        storedFlag = bestFit(procArgs);
    }else if (algorithmSelected == FIRSTFIT){
        storedFlag = firstFit(procArgs);
    }else if (algorithmSelected == WORSTFIT){
        storedFlag = worstFit(procArgs);
    }

    sem_post(&states->semProc);

    //DELETE FROM WAITING LIST
    sem_wait(&states->semWaitingList);
    deleteProcWaiting(id);
    sem_post(&states->semWaitingList);

    //ADD TO EXEC LIST

    if (storedFlag){

        sem_wait(&states->semFile);
        sleep(1);
        writeTable();
        //writeWaitingList();
        //writeExecList();
        sleep(1);
        sem_post(&states->semFile);


        sem_wait(&states->semExecProc);
        insertProcExec(id);
        sem_post(&states->semExecProc);

        sleep(procArgs->duration);
        
        sem_wait(&states->semProc);
        
        clearProc(id);
        
        sem_post(&states->semProc);

        //DELETE FROM EXEC LIST
        sem_wait(&states->semExecProc);
        deleteProcExec(id);
        sem_post(&states->semExecProc);
    }
    printf("Process id : %d ending \n", procArgs->id);

    sem_wait(&states->semFile);
    sprintf(buffer,"Process id : %d ending \n", procArgs->id);
    writeDataLog(buffer);
    sem_post(&states->semFile);

    threadsFlag[id] = false;

    pthread_exit(NULL);

}



//CREATES NEW PROCESSES IN A RANGE OF (20 - 60secs)
void generateProcesses(){
    size_t shmsize = sizeof(states_body);
    int waitingTime, shmid, flag;
    proc_args *procArgs;
    key_t key;
    char buffer[200];

    shmsize = sizeof(states_body) + sizeof(line_body)*pages + sizeof(proc_args)*pagesWaitingList 
        + sizeof(proc_args)*pagesExecList;

    if ((key = ftok(getPathKey(), 'R')) == -1){
        printf("File not found \n");
        return;
    }

    if ((shmid = shmget(key, shmsize, 0666)) == -1) {
        printf("Error smhget \n");
        return;
    }

    states = shmat(shmid, NULL, 0);
    procedures = shmat(shmid, NULL, 0) + sizeof(states_body);
    dataWaiting = shmat(shmid, NULL, 0) + sizeof(states_body) + sizeof(line_body)*pages;
    dataExecute = shmat(shmid, NULL, 0) + sizeof(states_body) + sizeof(line_body)*pages
        + sizeof(proc_args)*pagesWaitingList;
    
    if ((int)(uintptr_t)states == -1) {
        printf("Error shmgmat \n");
        return;
    }

    //END THE LAUNCHER
    states->start = true;
    states->procedureLaunched = true;
    //

    sem_wait(&states->semFile);
    sleep(1);
    writeTable();
    //writeWaitingList();
    //writeExecList();
    sleep(1);
    sem_post(&states->semFile);

    while (true){

        if (states->finish == true){
            break;
        }

        if (countP == 500){break;}

        procArgs = malloc(sizeof(proc_args));
        procArgs->id        = countP;
        procArgs->lines     = rand() %11+1;
        procArgs->duration  = rand() %50+20;//


        printf("Create process %d : lines %d : duration %d \n",procArgs->id , procArgs->lines,procArgs->duration);

        sem_wait(&states->semFile);
        sprintf(buffer,"Create process %d : lines %d : duration %d \n",procArgs->id , procArgs->lines,procArgs->duration);
        writeDataLog(buffer);
        sem_post(&states->semFile); 

        pthread_create(&(threadsID[countP]), NULL, simulateProc, procArgs);

        countP ++;

        printf("Total processes %d \n", countP);

        //WAIT TO CREATE NEW PROCESS
        waitingTime = 5;//rand() %10+5

        printf("Remaining time for next process %d\n",waitingTime );
        sem_wait(&states->semFile);
        sprintf(buffer,"Remaining time for next process %d\n",waitingTime );
        writeDataLog(buffer);
        sem_post(&states->semFile); 

        //-----------------------------

        sleep(waitingTime);

        //PRINT TABLE, DELETE LATER     ****
        sem_wait(&states->semProc);
        sem_wait(&states->semWaitingList);
        sem_wait(&states->semExecProc);
        //printWaitingList();
        //printExecList();
        sleep(1);
        printTable();
        sleep(1);
        sem_post(&states->semExecProc);
        sem_post(&states->semWaitingList);
        sem_post(&states->semProc);
        sleep(1);

    }

    for (int i = 0; i < countP; i++){
        if (threadsFlag[i]){
            flag = pthread_cancel(threadsID[i]);
            if (flag != 0)
                printf("Error canceling process id : %d\n", i);
        }
    }

    printf("Procedure task completed\n");
    
    states->finishReady = true;

    states->procedureLaunched = false;

    shmdt((void *) states);

    printf("Done ...\n");

    
}




int main(int argc, char const *argv[])
{
    pages = getTotalPages();

    if (pages == -1){printf("Error reading the shared memory. Load mainL first\n"); return 0;}

    pagesWaitingList = getTotalPagesWaitingList();
    pagesExecList = getTotalPagesExecList();
    countP = 0;

    if (argc > 1){
        algorithmSelected = atoi(argv[0]);
    }else{
        printf("Type the algorithm\n");
        printf("0 - Best fit\n");
        printf("1 - First fit\n");
        printf("2 - Worst fit\n");
        printf("3 - Exit\n");
        printf(">");
        scanf("%d", &algorithmSelected);
        printf("\n");
    }

    if (algorithmSelected == 3){return 0;}

    srand(time(NULL));

    generateProcesses();

    return 0;
}