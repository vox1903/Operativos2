#ifndef structure_H_
#define structure_H_
#include <stdbool.h>
#include <semaphore.h>

#include "linkedList.h"


typedef enum {BESTFIT, FIRSTFIT, WORSTFIT, EXIT} algorithm;

//ACTUAL LIENES OF THE SHARED MEMORY
typedef struct line{
    int id;
} line_body;


//SHARED MEMORY CONF DATA
typedef struct states{
    bool start;
    bool finish;
    bool finishReady;
    bool finishSpyReady;
    bool spyLaunched;
    bool procedureLaunched;
    sem_t semProc;
    sem_t semWaitingList;
    sem_t semExecProc;
    sem_t semFile;

} states_body;


typedef struct procArgs{
    int id;
    int duration;
    int lines;
} proc_args;


#endif