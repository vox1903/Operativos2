all: mainSim

mainSim:
	gcc -pthread -o mainL launcher.c commons.c
	gcc -pthread -o mainP procedure.c commons.c
	gcc -pthread -o mainS spy.c commons.c
	gcc -o mainF finisher.c commons.c
clean:
	rm -f main
