#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

#include "commons.h"
#include "structures.h"


//==========================GLOBAL==========================
states_body *states;
line_body *procedures;
proc_args *dataWaiting;
proc_args *dataExecute;
int pages,pagesWaitingList,pagesExecList;

//==========================






int main(int argc, char const *argv[]){
    size_t shmsize = sizeof(states_body);
    int waitingTime, shmid;
    proc_args *procArgs;
    key_t key;
    
    pages = getTotalPages();

    if (pages == -1){printf("Error reading the shared memory. Load mainL first\n"); return 0;}

    pagesWaitingList = getTotalPagesWaitingList();
    pagesExecList = getTotalPagesExecList();

    shmsize = sizeof(states_body) + sizeof(line_body)*pages + sizeof(proc_args)*pagesWaitingList 
        + sizeof(proc_args)*pagesExecList;

    if ((key = ftok(getPathKey(), 'R')) == -1){
        printf("File not found \n");
        return 0;
    }

    if ((shmid = shmget(key, shmsize, 0666)) == -1) {
        printf("Error smhget \n");
        return 0;
    }

    states = shmat(shmid, NULL, 0);
    procedures = shmat(shmid, NULL, 0) + sizeof(states_body);
    dataWaiting = shmat(shmid, NULL, 0) + sizeof(states_body) + sizeof(line_body)*pages;
    dataExecute = shmat(shmid, NULL, 0) + sizeof(states_body) + sizeof(line_body)*pages
        + sizeof(proc_args)*pagesWaitingList;
    
    if ((int)(uintptr_t)states == -1) {
        printf("Error shmgmat \n");
        return 0;
    }

    //END THE LAUNCHER
    states->finish = true;
    //

    while (states->finishReady == false && states->procedureLaunched == true){
        printf("Waiting procedure to finish process \n");
        sleep(1);
    }

    while (states->finishSpyReady == false && states->spyLaunched == true){
        printf("Waiting spy to finish process \n");
        sleep(1);
    }

    printf("Launcher task completed.\n");
    shmdt((void *) states);
    shmctl(shmid, IPC_RMID, NULL);

    printf("Done ...\n");

    return 0;
}