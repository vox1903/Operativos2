#ifndef commons_H_ 
#define commons_H_

char *getPathKey();

void writePages(char *pages,char *pagesWaitingList,char *pagesExecList);

int getTotalPages();

int getTotalPagesWaitingList();

int getTotalPagesExecList();

void clearPages();

char *getLog();

void writelnLog(char *data);

void writeDataLog(char *data);

void clearLog();

#endif // commons_H_