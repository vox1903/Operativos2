#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdint.h>

#include "commons.h"
#include "structures.h"

int main(int argc, char const *argv[]){

    line_body *data;
    proc_args *dataWaiting;
    proc_args *dataExecute;
    states_body *states;
    char buffer[32];
    int shmid , pages, pagesWaitingList, pagesExecList;
    key_t key;
    size_t shmsize;

    clearLog();

    if (argc > 1){
        //strcpy(buffer,argv[1]);
        printf("Number of pages accepted...\n");
    }else{
        printf("Type the number of pages \n");
        printf(">");
        scanf("%s", buffer);
        printf("\n");
    }

    //SAVE THE PAGES FOR THE OTHER PROCESSES
    writePages(buffer,buffer,buffer);
    pages = atoi(buffer);
    pagesWaitingList = pages;
    pagesExecList = pages;
    
    shmsize = sizeof(states_body) + sizeof(line_body)*pages + sizeof(proc_args)*pagesWaitingList 
        + sizeof(proc_args)*pagesExecList;

    //ASK FOR MEMORY
    if ((key = ftok(getPathKey(), 'R')) == -1){
        printf("File not found \n");
        return 0;
    }

    if ((shmid = shmget(key, shmsize, 0666 | IPC_CREAT)) == -1) {
        printf("Error smhget \n");
        return 0;
    }

    states = shmat(shmid, NULL, 0);
    data = shmat(shmid, NULL, 0) + sizeof(states_body);
    dataWaiting = shmat(shmid, NULL, 0) + sizeof(states_body) + sizeof(line_body)*pages;
    dataExecute = shmat(shmid, NULL, 0) + sizeof(states_body) + sizeof(line_body)*pages
        + sizeof(proc_args)*pagesWaitingList;

    if ((int)(uintptr_t)data == -1) {
        printf("Error shmgmat \n");
        return 0;
    }

    //SET VALUES
    for (int i = 0; i < pages; i++){
        data[i].id = -1;
    }

    //SET VALUES WAITING LIST
    for (int i = 0; i < pagesWaitingList; i++){
        dataWaiting[i].id = -1;
    }

    //SET VALUES EXEC LIST
    for (int i = 0; i < pagesExecList; i++){
        dataExecute[i].id = -1;
    }

    states->start = false;
    states->finish = false;
    states->finishReady = false;
    states->finishSpyReady = false;
    states->spyLaunched = false;
    states->procedureLaunched = false;  

    sem_init(&states->semProc, 0, 1);
    sem_init(&states->semWaitingList, 0, 1);
    sem_init(&states->semExecProc, 0, 1);
    sem_init(&states->semFile, 0, 1);

    while (states->start == false){
        printf("Waiting another process \n");
        sleep(1);
    }

    printf("Launcher task completed.\n");
    shmdt((void *) states);

    printf("Done ...\n");


    return 0;
}