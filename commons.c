#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "commons.h"

#define LOGNAMEFILE "logName.txt"
#define SHAREDKEY "key.txt"
#define PAGES "pages.txt"

typedef struct proc{
    
    int timeGap;
    int space;
    int tid;

} proc_body;


char *getPathKey(){
    char fileName[32];
    char line[32];
    char *keyName = malloc(32);
    FILE* file;

    strcpy(fileName,SHAREDKEY);

    file = fopen(fileName, "r");

    fgets(line, sizeof(line), file);

    strcpy(keyName, line);

    fclose(file);

    return keyName;
}


int getTotalPages(){
    char fileName[32];
    char line[32];
    int value;
    FILE* file;

    strcpy(fileName,PAGES);

    file = fopen(fileName, "r");

    if (file == 0 ){
        printf("\n========= ERROR READING ON PAGES ========= \n");
        return -1;
    }

    fgets(line, sizeof(line), file);

    value = atoi(line);

    fclose(file);

    return value;
}

int getTotalPagesWaitingList(){
    char fileName[32];
    char line[32];
    int value;
    FILE* file;

    strcpy(fileName,PAGES);

    file = fopen(fileName, "r");

    if (file == 0 ){
        printf("\n========= ERROR READING ON PAGES ========= \n");
        return -1;
    }

    fgets(line, sizeof(line), file);

    fgets(line, sizeof(line), file);

    value = atoi(line);

    fclose(file);

    return value;
}

int getTotalPagesExecList(){
    char fileName[32];
    char line[32];
    int value;
    FILE* file;

    strcpy(fileName,PAGES);

    file = fopen(fileName, "r");

    if (file == 0 ){
        printf("\n========= ERROR READING ON PAGES ========= \n");
        return -1;
    }

    fgets(line, sizeof(line), file);

    fgets(line, sizeof(line), file);

    fgets(line, sizeof(line), file);

    value = atoi(line);

    fclose(file);

    return value;
}

void writePages(char *pages,char *pagesWaitingList,char *pagesExecList){

    FILE* file;

    file = fopen(PAGES, "w");

    if (file == 0 ){
        printf("\n========= ERROR WRITING ON PAGES ========= \n");
        return;
    }
        
    fprintf(file,"%s\n",pages);
    fprintf(file,"%s\n",pagesWaitingList);
    fprintf(file,"%s\n",pagesExecList);
    fclose(file);

}

void clearPages(){

    char *fileName = PAGES;
    fclose(fopen(fileName, "w"));    

    printf("\n========= CLEAR LOG ========= \n");
}



char *getLog(){
    char fileName[32];
    char line[32];
    char *logName = malloc(32);
    FILE* file;

    strcpy(fileName,LOGNAMEFILE);

    file = fopen(fileName, "r");

    fgets(line, sizeof(line), file);

    strcpy(logName, line);

    fclose(file);

    return logName;
}



void writeDataLog(char *data){

    char buffer[250];

    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    sprintf(buffer,"%s",asctime (timeinfo));
    strcat(buffer,data);

	char *fileName = getLog();
    FILE* file;

    file = fopen(fileName, "a");

	if (file == 0 ){
		printf("\n========= ERROR WRITING ON LOG ========= \n");
		return;
	}
		
	fprintf(file,"%s\n",buffer);
	fclose(file);

}


void writelnLog(char *data){
    char *fileName = getLog();
    FILE* file;

    file = fopen(fileName, "a");

    if (file == 0 ){
        printf("\n========= ERROR WRITING ON LOG ========= \n");
        return;
    }
        
    fprintf(file,"%s\n",data);
    fclose(file);

}


void clearLog(){

    char *fileName = getLog();
    fclose(fopen(fileName, "w"));	

    printf("\n========= CLEAR LOG ========= \n");
}


// void printLog(){

//     char line[256];
//     char *fileName = getLog();
//     FILE* file;

//     file = fopen(fileName, "r");

//     printf("\n========= BEGIN FILE ========= \n");
//     while(fgets(line, sizeof(line), file)){
//         printf("Line : %s\n", line);
//     }
//     printf("\n========= END FILE ========= \n");
//     fclose(file);
// }

