## Dinamic Process Simulator

## Installation

Use a Linux enviorment. 
make
./mainL		Main program run before the rest, asks how many segments of memory are requested
./mainP		Generates processes at random after the algorithm is selected
./mainS		Shows how the memory is used depending on what the user requested
./mainF		Stops all other programs


## Contributors

Juan Jose Guerrero Madrigal
Jostin Marin Mena

## TEC
Course: Sistemas Operativos
Professor: Ericka Marin Schumann
Year: 2017
