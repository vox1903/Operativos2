#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

#include "commons.h"
#include "structures.h"

//==========================GLOBAL==========================
states_body *states;
line_body *procedures;
proc_args *dataWaiting;
proc_args *dataExecute;
int pages, algorithmSelected, countP,pagesWaitingList, pagesExecList, optionSelected;

//==========================


void printTable(){

    printf("============TABLE============\n");
    for (int i = 0; i < pages; i++){
        printf("Line %d : Process id %d \n", i , procedures[i].id );
    } 
    printf("============END TABLE============\n");

}

void printWaitingList(){

    printf("============WAITING LIST============\n");
    for (int i = 0; i < pagesWaitingList; i++){
        if (dataWaiting[i].id != -1){
            printf("Line %d : Process id %d \n", i , dataWaiting[i].id );    
        }
        
    } 
    printf("============END TABLE============\n");

}

void printExecList(){

    printf("============EXECUTION TABLE============\n");
    for (int i = 0; i < pagesExecList; i++){
        if (dataExecute[i].id != -1  ){
            printf("Line %d : Process id %d \n", i , dataExecute[i].id );
        }
        
    } 
    printf("============END TABLE============\n");

}

void printFileState(){
    
    sem_wait(&states->semProc);
    printf("============FILE STATE============\n");
    
    char line[256];
    char *fileName = getLog();
    FILE* file;

    file = fopen(fileName, "r");

    while(fgets(line, sizeof(line), file)){
        printf("%s\n", line);
    }
    printf("\n========= END FILE ========= \n");
    fclose(file);

    printf("========================\n");
    sem_post(&states->semProc);

}


int main(int argc, char const *argv[]){
    size_t shmsize = sizeof(states_body);
    int  shmid,check;
    key_t key;
    pages = getTotalPages();

    if (pages == -1){printf("Error reading the shared memory. Load mainL first\n"); return 0;}

    shmsize = sizeof(states_body) + sizeof(line_body)*pages;

    if ((key = ftok(getPathKey(), 'R')) == -1){
        printf("File not found \n");
        return 0;
    }

    if ((shmid = shmget(key, shmsize, 0666)) == -1) {
        printf("Error smhget \n");
        return 0;
    }

    states = shmat(shmid, NULL, 0);
    procedures = shmat(shmid, NULL, 0) + sizeof(states_body);
    dataWaiting = shmat(shmid, NULL, 0) + sizeof(states_body) + sizeof(line_body)*pages;
    dataExecute = shmat(shmid, NULL, 0) + sizeof(states_body) + sizeof(line_body)*pages
        + sizeof(proc_args)*pagesWaitingList;
    
    if ((int)(uintptr_t)states == -1) {
        printf("Error shmgmat \n");
        return 0;
    }

    if (!states->start){
        printf("Start mainP before launching mainS...\n");
        return 0;
    }

    states->spyLaunched = true;

    while (true){

        if (states->finish == true){
            break;
        }

        printf("Type the algorithm\n");
        printf("0 - Print Table\n");
        printf("1 - File state\n");
        printf("2 - Processes states\n");
        printf("3 - Exit\n");
        printf(">");
        scanf("%d", &optionSelected);
        printf("\n");

        if (states->finish == true){
            break;
        }

        if (optionSelected == 3){break;}

        switch (optionSelected){
            case 0:
                printTable();
                break;
            case 1:
                printFileState();
                break;
            case 2:
                sem_wait(&states->semProc);
                sem_wait(&states->semWaitingList);
                sleep(1);
                printExecList();
                printWaitingList();
                sleep(1);
                sem_post(&states->semExecProc);
                sem_post(&states->semWaitingList);
                break;

        }
    }


    printf("Spy task completed\n");

    states->finishSpyReady = true;
    states->spyLaunched = false;

    shmdt((void *) states);

    printf("Done ...\n");


    return 0;
}